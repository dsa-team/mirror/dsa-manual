========
Glossary
========

.. if you add new entries, keep the alphabetical sorting!

.. glossary::

  DMUP
    DMUP stands for Debian Machine Usage Policy. One of the duties of
    the Debian Systems Administration Team is to maintain the Debian
    Machine Usage Policies, within the following limits:

    - that the DMUP cannot directly cause the expulsion of a developer 
      from the project; it can however propose the developer for 
      expulsion to DAM, on the basis of DMUP violation.

    - changes to the DMUP shall be announced to the debian-devel-announce
      mailing list at least 2 months in advance with respect to when 
      they are supposed to become effective.

  DPL
    Debian Project Leader, as per §5 of the Debian Constitution.

  SSO
    Single Sign On, a system designed to minimize the number of times that a user must log into multiple applications

.. vim: set textwidth=72 syntax=rst :
.. Local Variables:
.. mode: rst
.. fill-column: 72
.. End:

