Intention of this document
==========================
This document tries to describe the operations needed for the day-by-day
business of the debian.org machines. It will never be complete and
up-to-date as things are changing quite fast, but it tries to give you a
first impression how things could work.

It also should help new DSA members to find their way into the team by
taking over regular tasks. 

.. vim: set textwidth=72 syntax=rst :
.. Local Variables:
.. mode: rst
.. fill-column: 72
.. End:

