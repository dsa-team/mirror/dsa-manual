.. DSA Manual documentation master file, created by
   sphinx-quickstart on Sat Sep 17 21:28:33 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DSA Manual!
======================

Contents:

.. toctree::
   :maxdepth: 2

   intention.rst
   first-steps.rst
   services.rst
   glossary.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. vim: set textwidth=72 :
