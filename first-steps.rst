.. first-steps.rst
   Copyright (C) 2011 Martin Zobel-Helas


First steps as new DSA member
=============================
As new DSA member there are a few things you should probably do, but you
may not know all, thus i will list those here.

- Set yourself a sudo password on `db.debian.org
  <https://db.debian.org/login.html>`_, this is needed so you can access
  your root privileges.
- Checkout a few common GIT repositories (you might know some of them
  already and have them checked out from `git.debian.org
  <http://git.debian.org/>`_ but you want them from the original source)

  - `The debian.org metapackage <git+ssh://$USER@db.debian.org/git/debian.org.git/>`_
  - `domains <git+ssh://$USER@dns.debian.org/git/domains.git/>`_
  - `dsa-geodomains
    <git+ssh://$USER@dns.debian.org/git/dsa-geodomains.git>`_
  - `dsa-nagios <git+ssh://$USER@db.debian.org/git/dsa-nagios.git/>`_
  - `dsa-puppet
    <git+ssh://$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git/>`_
  - `dsa-wiki <git+ssh://$USER@db.debian.org/git/dsa-wiki.git/>`_
  - `guest-keyring <git+ssh://$USER@db.debian.org/git/guest-keyring.git/>`_
  - `pwstore <http://svn.noreply.org/git/pwstore.git>`_
  - `dsa-passwords
    <git+ssh://$USER@spohr.debian.org/git/dsa-passwords.git/>`_

.. vim: set textwidth=72 syntax=rst :
.. Local Variables:
.. mode: rst
.. fill-column: 72
.. End:


